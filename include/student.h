#ifndef STUDENT_H
#define STUDENT_H
#include <string>
#include <map>
#include <iostream>
using namespace std;
class Student
{
private:
    struct student_record
    {
        string roll_no;
        int age;
        float cgpa;
    } self;

    map<string, int> resultMap;

public:
    Student(string roll_no, int age, float cgpa);
    int get_age();
    float get_cgpa();
    string get_rollnum();
    int get_subject_marks(string subject);
    void set_subject_marks(string subject, int marks);
    void print_all_marks();
    ~Student();
};
#endif