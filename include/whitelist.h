#ifndef WHITELIST_H
#define WHITELIST_H
#include <map>
#include <list>
#include "student.h"
class whitelist
{
private:
    std::map<std::string, Student *> student_record;
    std::list<Student> student_data_list;

public:
    void add_to_whitelist(std::string studentName, const Student &student);
    bool is_student_present(std::string studentName) const;
    Student *get_student_data(const std::string &studentName) const;
};

#endif