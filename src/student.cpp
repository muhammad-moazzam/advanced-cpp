#include <iostream>
#include "student.h"
#include <map>
#include <string>

using namespace std;

Student::Student(string roll_no, int age, float cgpa)
{
    self.roll_no = roll_no;
    self.age = age;
    self.cgpa = cgpa;
}

int Student::get_age()
{
    return self.age;
}

float Student::get_cgpa()
{
    return self.cgpa;
}
void Student::set_subject_marks(string subject, int marks)
{
    resultMap[subject] = marks;
}

int Student::get_subject_marks(string subject)
{
    map<string, int>::iterator itr = resultMap.find(subject);
    if (itr != resultMap.end())
    {
        return itr->second;
    }
    else
    {
        return -1;
    }
}

void Student::print_all_marks()
{
    map<string, int>::iterator itr;
    if (resultMap.begin() == resultMap.end())
    {
        cout << "No subject is registered";
    }
    else
    {
        for (itr = resultMap.begin(); itr != resultMap.end(); itr++)
        {
            cout << "Subject: " << (*itr).first << " Marks: " << (*itr).second << "\n";
        }
    }
}
string Student::get_rollnum()
{
    return self.roll_no;
}

Student::~Student()
{
    //cout << "Destructor is called" << endl;
}
