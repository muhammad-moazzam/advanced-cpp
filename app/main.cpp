#include <iostream>
#include "student.h"
#include "whitelist.h"
using namespace std;
int main()
{

    string stdName;
    Student studentObj1("123_a", 210, 2.1);
    Student studentObj2("123_b", 211, 2.11);
    Student studentObj3("123_c", 212, 2.12);
    Student studentObj4("123_d", 213, 2.13);
    Student studentObj5("123_e", 214, 2.14);
    Student studentObj6("123_f", 215, 2.15);
    Student studentObj7("123_g", 216, 2.16);
    Student studentObj8("123_h", 217, 2.17);
    Student studentObj9("123_i", 218, 2.18);
    Student studentObj10("123_k", 219, 2.19);
    Student studentObj11("123_l", 220, 2.1);
    Student studentObj12("123_m", 221, 2.12);
    Student studentObj13("123_n", 222, 2.13);
    Student studentObj14("123_o", 213, 2.14);
    Student studentObj15("123_p", 214, 2.15);
    Student *stdptr;

    whitelist whitelistObj;
    whitelistObj.add_to_whitelist("Hermes", studentObj1);
    whitelistObj.add_to_whitelist("Artemis", studentObj2);
    whitelistObj.add_to_whitelist("Ares", studentObj3);
    whitelistObj.add_to_whitelist("Poseidon", studentObj4);
    whitelistObj.add_to_whitelist("Zeus", studentObj5);
    whitelistObj.add_to_whitelist("Hades", studentObj6);
    whitelistObj.add_to_whitelist("Morheus", studentObj7);
    whitelistObj.add_to_whitelist("Hephaestus", studentObj8);
    whitelistObj.add_to_whitelist("Hera", studentObj9);
    whitelistObj.add_to_whitelist("Chronos", studentObj10);
    whitelistObj.add_to_whitelist("Odin", studentObj11);
    whitelistObj.add_to_whitelist("Thor", studentObj12);
    whitelistObj.add_to_whitelist("Frey", studentObj13);
    whitelistObj.add_to_whitelist("Tyr", studentObj14);
    whitelistObj.add_to_whitelist("Sif", studentObj15);

    studentObj1.set_subject_marks("Science", 30);
    studentObj1.set_subject_marks("Maths", 40);
    studentObj1.print_all_marks();
    cin >> stdName;

    if (whitelistObj.is_student_present(stdName))
    {
        stdptr = whitelistObj.get_student_data(stdName);
    }
    cout << stdptr->get_age() << endl;
    cout << stdptr->get_cgpa() << endl;
    cout << stdptr->get_rollnum() << endl;
}